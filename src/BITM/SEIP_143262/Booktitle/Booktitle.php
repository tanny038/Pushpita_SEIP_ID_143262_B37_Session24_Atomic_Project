<?php

namespace App\Booktitle;

use App\Model\Database as DB;

use PDO;

//include("Database.php");

class Booktitle extends DB
{

    public $conn;

    public $id = "";

    public $book_title = "";

    public $author_name = "";


    /*public function __construct()
    {

        parent::__construct();

    }*/

   /* public function index()
    {


        $conn = $this->DBH;
        $STH = $conn->prepare("select * from book_title");
        $STH->execute();

        $STH->setFetchMode(PDO::FETCH_ASSOC);
        while ($row = $STH->fetch()) {

            echo "<br/>" . $row['id'] . "<br/>";

            echo $row['book_name'] . "<br/>";

            echo $row['author_name'] . "<br/>";

        }*/
    public function setData($data=NULL){

        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }

        if(array_key_exists('book_title',$data)){
            $this->book_title = $data['book_title'];
        }

        if(array_key_exists('author_name',$data)){
            $this->author_name = $data['author_name'];
        }
    }

    public function store(){
        $arrData = array($this->book_title,$this->author_name);

       $sql = "insert into book_title(book_title,author_name) values(?,?)";
        echo $sql;

        $STH = $this->DBH->prepare($sql);
        $STH->execute($arrData);
    }




}