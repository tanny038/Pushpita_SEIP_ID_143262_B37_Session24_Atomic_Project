<?php

include_once('../../../vendor/autoload.php');

use App\Booktitle\Booktitle;

$objBT = new Booktitle();
$objBT->setData($_POST);

echo $objBT->book_title;
echo $objBT->author_name;