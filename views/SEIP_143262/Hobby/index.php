<?php
require_once("../../vendor/autoload.php");

use App\Database;
use App\Birthday;
use App\Booktitle;
use App\City;
use App\Email;
use App\Gender;
use App\Hobby;
use App\ProfilePic;
use App\Summary;

$obj1=new Birthday();
$obj2=new Booktitle();
$obj3=new City();
$obj4=new Email();
$obj5=new Gender();
$obj6=new Hobby();
$obj7=new ProfilePic();
$obj8=new Summary();

echo "<br> Birthday </br> <hr>";
$obj1->index();

echo "<br> Booktitle </br> <hr>";
$obj2->index();

echo "<br> City </br> <hr>";
$obj3->index();

echo "<br> Email </br> <hr>";
$obj4->index();

echo "<br> Gender </br> <hr>";
$obj5->index();

echo "<br> Hobby </br> <hr>";
$obj6->index();

echo "<br> Profile Picture </br> <hr>";
$obj7->index();

echo "<br> Summary Of Organizatoin </br> <hr>";
$obj8->index();